<?php

namespace App\Http\Controllers;

use App\Models\Ad;
use App\Models\Category;
use App\Models\Message;
use App\Models\User;
use Illuminate\Contracts\Session\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'verified'], ['except' => ['welcome','contactForm','home','aboutMe']]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = Auth::user();
        $user_image = Auth::user()->user_image;
        $user_ads = Ad::all()->where('user_id', $user->id);

        //dd($user);

        return view('home',compact('user','user_image','user_ads'));
    }

    public function welcome()
    {
        return view('welcome');
    }
    public function aboutMe()
    {
        return view('aboutMe');
    }

}
