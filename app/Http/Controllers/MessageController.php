<?php

namespace App\Http\Controllers;

use App\Models\Ad;
use App\Models\Message;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MessageController extends Controller
{
    public function userMessage(Request $request, $id)
   {
             $user = Auth::user();
             $ad_name = Ad::find($id)->title;

             $request->validate([

                 'body' => 'required',

             ]);

             // New Message

            $messages = new Message();

            $messages->title = $request->title;
            $messages->ad_name = $ad_name;
            $messages->body = $request->body;
            $messages->sender_id = auth()->user()->id;
            $messages->receiver_id = Ad::find($id)->user_id;
            $messages->ad_id = Ad::find($id)->id;
            $messages->save();



            return redirect()->back()->with('message', 'Your message is sent to owner of this ad. ');

    }

    public function showMessages()
    {

        $user = Auth::user();
        $messages = Message::all()->where('receiver_id', Auth::user()->id);

        return view('showUserMessages', compact('messages','user'));
    }

    public function replayMsg()
    {
        $user = Auth::user();
        $sender_id = request()->sender_id;
        $ad_id = request()->ad_id;
        $ad_name = request()->ad_name;


        $messages = Message::where('sender_id',$sender_id)->where('ad_id',$ad_id)->where('ad_name', $ad_name)->get();

        return view('replayMsg', compact('sender_id','ad_id','messages','user','ad_name'));
    }

    public function replayMsgStore(Request $request)
    {
        $sender = User::find($request->sender_id);
        $ad = Ad::find($request->ad_id);
       // $ad_name = Ad::find($request->ad_name);
         $request->validate([

        'body' => 'required',

        ]);

        // New message

        $message = new Message();
        $message->title = $request->title;
        $message->ad_name = $ad->title;
        $message->body = $request->body;
        $message->sender_id = Auth::user()->id;
        $message->receiver_id = $sender->id;
        $message->ad_id = $ad->id;
        $message->save();

        return redirect()->route('showUserMessages')->with('message', 'Replay sent.');


    }

    public function deleteMsg($id)
    {
        $message = Message::find($id);
        $message->delete();

        return redirect()->back()->with('message', 'Message is deleted.');


    }
}
