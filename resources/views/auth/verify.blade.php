@extends('layouts.app')

@section('title')
    Email verification
@endsection

@section('content-title')
<div class="container justify-content-center">
    <h1 class="text-light">Email verification</h1>
</div>
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card  verify-email">
                <div style="background-color: brown" class="card-header text-light" class="card-header">{{ __('Verify Your Email Address') }}</div>

                <div class="card-body text-light">
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ __('A fresh verification link has been sent to your email address.') }}
                        </div>
                    @endif

                    {{ __('Before proceeding, please check your email for a verification link.') }}
                    {{ __('If you did not receive the email') }},
                    <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
                        @csrf
                        <button type="submit" class="btn btn-link p-0 m-0 align-baseline">{{ __('click here to request another') }}</button>.
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
