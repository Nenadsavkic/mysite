<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes(['verify'=>true]);

Route::get('/', [App\Http\Controllers\HomeController::class, 'welcome'])->name('welcome');    // Pocetna (biografija)
Route::get('/aboutMe', [App\Http\Controllers\HomeController::class, 'aboutMe'])->name('aboutMe'); // Biografija engleski
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');        // Home strana profil korisnika
Route::get('/home-single-ad/{id}', [App\Http\Controllers\AdController::class, 'showSingleAd']) // Single ad korisnika
->name('singleAd');
Route::get('/showAllUsers', [App\Http\Controllers\UsersController::class, 'showAllUsers'])  // Prikaz svih korisnika (Usera)
->name('showAllUsers');


Route::get('/home-messages', [App\Http\Controllers\MessageController::class, 'showMessages']) // Prikaz poruke korisnika
->name('showUserMessages');
Route::get('/cars', [App\Http\Controllers\CarsController::class, 'index'])->name('cars');  // Prikaz svih oglasa iz kategorije 'Cars'
Route::get('/computers', [App\Http\Controllers\ComputersController::class, 'index'])->name('computers'); // Prikaz svih oglasa 'Computers'
Route::get('/phones', [App\Http\Controllers\PhonesController::class, 'index'])->name('phones'); // Prikaz svih oglasa 'Phones'
Route::get('/contact', [App\Http\Controllers\ContactController::class, 'contactForm'])             // Kontakt forma
->name('contactForm');
Route::get('/user-deleteImg/{id}', [App\Http\Controllers\UsersController::class, 'deleteImg'])  // Dugme za brisanje profilne slike
->name('user.deleteImg');
Route::get('/home-ad-form', [App\Http\Controllers\AdController::class, 'showAdForm'])        // Forma za unos oglasa
->name('showAdForm');
Route::get('/home-editAd-form/{id}', [App\Http\Controllers\AdController::class, 'editAdForm']) // Form za editovanje oglasa
->name('adEditForm');

Route::get('/ad-single-ad/{id}', [App\Http\Controllers\AdController::class, 'index'])         // Prikaz single oglasa iz bilo koje kategorije
->name('ad.singleAd');

Route::get('/home-replay', [App\Http\Controllers\MessageController::class, 'replayMsg'])          // odgovor na poruku
->name('replayMsg');
Route::post('/user-saveImg/{id}', [App\Http\Controllers\UsersController::class, 'saveImg'])   // Postavljanje profilne slike
->name('user.saveImg');
Route::get('/allUserAds/{id}', [App\Http\Controllers\AdController::class, 'allUserAds'])         // Prikaz svih korisnikovih oglasa
->name('allUserAds');





Route::post('/home-save-ad', [App\Http\Controllers\AdController::class, 'saveAd'])         // Postavljanje oglasa
->name('saveAd');

Route::delete('/home-ad-delete/{id}', [App\Http\Controllers\AdController::class, 'adDelete'])  // Brisanje oglasa
->name('adDelete');

Route::post('/home-save-editedAd/{id}', [App\Http\Controllers\AdController::class, 'saveEditedAd']) // Cuvanje editovanog oglasa
->name('saveEditedAd');

Route::post('/home-user-message/{id}', [App\Http\Controllers\MessageController::class, 'userMessage'])  // Poruke korisnika
->name('userMessage');

Route::post('/home-replay-store', [App\Http\Controllers\MessageController::class, 'replayMsgStore'])   // Cuvanje odgovora na poruku
->name('replayMsgStore');

Route::delete('/home-message-delete/{id}', [App\Http\Controllers\MessageController::class, 'deleteMsg'])  // brisanje poruke
->name('deleteMsg');
Route::post('/contact', [App\Http\Controllers\ContactController::class, 'contactUsForm'])  // kontakt store
->name('contact.store');
Route::delete('/deleteProfile/{id}', [App\Http\Controllers\UsersController::class, 'deleteProfile'])  // Brisanje korisnika (Usera)
->name('deleteProfile');




